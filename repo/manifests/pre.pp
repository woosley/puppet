class repo::pre {

    file {"yum_repos_d":
        path => "/etc/yum.repos.d",
        source => "puppet:///modules/repo/empty",
        ensure => directory,
        recurse => true,
        purge => true,
        force => true,
        mode => 0755,
        owner => root,
        group => root,
        notify => Exec['yum_clean'],
    }

    exec {"yum_clean":
        command => "/usr/bin/yum clean all",
        refreshonly => true,
    }

}
