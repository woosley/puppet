# Class repo
# This module manage repos
#
# This module exports some virtual repos to be realized including internal HSBC
# repo and external repos
#
# For external repo, since there is no direct http access, you have to set a
# facter/external parameter *proxy* to setup the yum http proxy.
#
# Sample Usage:
# include repo
# realize Yum::Repo['internal_epel']

class repo {
    include repo::pre

    @repo::yumrepo {"maven":
        enabled => 1,
        gpgcheck => 0,
        desc => "maven from apache foundation.",
        baseurl => 'http://repos.fedorapeople.org/repos/dchen/apache-maven/epel-6/x86_64/',
        proxy => $proxy,
        skip_if_unavailable => 1,
    }

    @repo::yumrepo {"maven-source":
        enabled => 1,
        gpgcheck => 0,
        desc => "maven from apache foundation -- source",
        baseurl => 'http://repos.fedorapeople.org/repos/dchen/apache-maven/epel-6/SRPMS',
        proxy => $proxy,
        skip_if_unavailable => 1,
    }

    @repo::yumrepo {"nginx":
        enabled => 1,
        gpgcheck => 0,
        desc => "nginx repo",
        proxy => $proxy,
        baseurl => 'http://nginx.org/packages/rhel/6/x86_64/',
    }

    @repo::yumrepo {"epel":
        enabled => 1,
        gpgcheck => 0,
        desc => "epel",
        mirrorlist=> 'https://mirrors.fedoraproject.org/metalink?repo=epel-6&arch=$basearch',
        proxy => $proxy,
    }

    @repo::yumrepo {"node":
        enabled => 1,
        gpgcheck => 0,
        desc => "nodejs",
        baseurl => 'https://rpm.nodesource.com/pub_6.x/el/6/$basearch',
        proxy => $proxy,
    }
}
