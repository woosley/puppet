define repo::yumrepo(
    $desc = "No description set",
    $baseurl = "",
    $mirrorlist = "",
    $enabled = 0,
    $gpgcheck = 0,
    $gpgkey = "",
    $failovermetho = "",
    $priority = 99,
    $exclude = "",
    $proxy = "",
    $skip_if_unavailable=0,
    $includepkgs = ""){

    $filename = "/etc/yum.repos.d/${name}.repo"
    file {$filename:
        ensure => file,
        require =>  File['yum_repos_d'],
        mode => 0644, 
        owner => root,
        group => root,
        content => template("repo/default.repo"),
        notify => Exec['yum_clean'],
    }

    if $gpgcheck != '' {
        exec {"rpmkey_add_${name}":
            command => "/bin/rpm --import ${gpgkey}",
            before => File[$filename],
            refreshonly => true,
        }
    }
}
