class repo::withproxy {
  include repo
  realize Repo::Yumrepo['epel']
  realize Repo::Yumrepo['maven']
  realize Repo::Yumrepo['nginx']
  realize Repo::Yumrepo['node']
}
