class repo::internal {
  ## this is a repo configuration puppet for internal environments
  include repo
  realize Repo::Yumrepo['internal_epel', 'internal_puppet', 'internal_nginx', "internal_maven", "maddog", "internal_node"]
  if $::operatingsystem == 'CentOS' {
    realize Repo::Yumrepo['nexus']
  }
}
