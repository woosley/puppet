class profile::test {
  include profile::ngnix
  include profile::base
  Class['profile::base'] -> Class['profile::ngnix']
}
